extern crate force_remove;

use std::process::Command;
use force_remove::*;

fn main(){
    let mut args = std::env::args();

    args.next();

    let mut url_arg = None;

    for arg in args{
        if arg != "cargo" && arg != "build-git" {
            url_arg = Some(arg);
            break;
        }
    }

    let url = url_arg.unwrap_or_else(||{
        eprintln!("Error: Missing Git link");
        std::process::exit(-1);
    });

    Command::new("git").args(&["clone",&url]).status().unwrap_or_else(|_|light_panic());

    let project_name : String = if url.ends_with(".git") { url.chars().rev().skip(4).take_while(|c| *c != '/' ).collect::<Vec<char>>().into_iter().rev().collect() } else { url.chars().rev().take_while(|c| *c != '/' ).collect::<Vec<char>>().into_iter().rev().collect() };

    Command::new("cargo").current_dir(&project_name).args(&["build","--release"]).status().unwrap_or_else(|_|light_panic());

    std::fs::rename(format!("{}/target/release/", &project_name),format!("./{}-release", &project_name)).unwrap_or_else(|_|light_panic());

    force_remove_dir_all(&project_name).unwrap_or_else(|_|light_panic());

    println!("Finished!");
}

fn light_panic() -> !{
    eprintln!("Error: failed to sucessfully execute sub command(s)");
    std::process::exit(-2);
}