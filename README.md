Given a Git repository link to a Cargo project, builds it and outputs the release folder.

Installation:
```sh
cargo install cargo-build-git
```

Usage:
```sh
cargo build-git https://gitlab.com/XBagon/cargo-build-git.git
```